uc_viva_payments v0.01


INSTALL:
copy to modules folder and enable module


==============================================================================================
!IMPORTANT
==============================================================================================
To use NORMAL MODE instead of DEMO MODE(DEFAULT) you need to change the following line in uc_viva_payments.module:
define('GLOBAL_TESTMODE',1); ==> DEMO MODE
define('GLOBAL_TESTMODE',0); ==> NORMAL MODE (actual payment mode)
==============================================================================================

*configure settings (per user profile) in http://yourdomain/?q=admin/store/settings/payment/method/Vivapayments
*supports uc_recurring_payments
*multi-user payments (each product can be purchased from the viva account of the node-product creator)


vivapayments.com - website options for Viva source code:
payment success callback: cart/vivapayments/creditcardsuccess
payment failure callback: cart/vivapayments/creditcardfail
