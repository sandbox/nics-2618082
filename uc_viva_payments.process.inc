<?php
// $Id$

function uc_viva_payments_order_failure(){
    // Get the post data from Viva Payments.
  $data = $_REQUEST;

  if (empty($data['s'])) {
    drupal_set_message(t('Unexpected Error! Please contact the store owner.'), 'error');
    drupal_goto('<front>');
  }

  drupal_set_title(t('Payment not Accepted'));

    return array(
    '#markup' => t('There was an error with your payment. You may either try
      again to complete payment <a href="@link">following this link</a>, or
      contact your bank and ask for payment authorization. Alternately use a
      different credit card.', array('@link' => url('cart/checkout/'))),
  );
}

function uc_viva_payments_order_success(){
  // Get the data from Viva Payments.
  $data = $_REQUEST;

  // Check if we have a result.
  if (empty($data['s'])) {
    drupal_set_message(t('Unexpected Error! Please contact the store owner.'), 'error');
    drupal_goto('<front>');
  }

  // Wait 3 seconds so as the order manage to change status.
  sleep(3);

  // Get order info.
  $ordercode = $data['s'];
  $transaction=$data['t'];
  $uc_orderid=db_query("SELECT order_id from uc_viva_payments WHERE ordercode=$ordercode")->fetchField();
  $order = uc_order_load($uc_orderid);
  //TODO: the below line was commented because it created empty users! was it needed? not in the first tests!
  //uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  uc_payment_enter($order->order_id, 'uc_viva_payments', $order->order_total, 0, $transaction, t('Paid by Vivapayments'));
  uc_order_comment_save($order->order_id, 0, t('Payment received from vivapayments with transaction id ').$transaction, 'admin');
  try{
  	db_merge('uc_viva_payments')
  	->key(array('order_id' => $order->order_id))
  	->fields(array(
  			'trans_id' => $transaction,
  			'status' => 'completed',
  	))->execute();
  }
  catch (PDOException $e) {
  	watchdog('error','PDO Error Message: %message',array('%message'=>$e->getMessage()),WATCHDOG_ERROR);
  }
  
  //check for recurring products
  $recurring=false;
  foreach ($order->products as $product)
  {
  	$prodfeatures=uc_product_feature_load_multiple($product->nid);
  	foreach ($prodfeatures as $feature){
  		if ($feature->fid==='recurring')
  		{
  			$recurring=true;
  		}
  	}
  }

  if ($recurring) uc_recurring_product_process_order($order);
  
  $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
 }